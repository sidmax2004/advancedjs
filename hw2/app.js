
// Може використовуватись коли ми самі зробили помилку в коді, або неправильної відповіді сервера.



  try {
    const div = document.querySelector('#root');
    const books = [
        { 
            author: "Люсі Фолі",
            name: "Список запрошених",
            price: 70 
          }, 
          {
           author: "Сюзанна Кларк",
           name: "Джонатан Стрейндж і м-р Норрелл",
          }, 
          { 
            name: "Дизайн. Книга для недизайнерів.",
            price: 70
          }, 
          { 
            author: "Алан Мур",
            name: "Неономікон",
            price: 70
          }, 
          {
           author: "Террі Пратчетт",
           name: "Рухомі картинки",
           price: 40
          },
          {
           author: "Анґус Гайленд",
           name: "Коти в мистецтві",
          }
    ];
    books.forEach(function (book, i) {
        const li = document.createElement('li')
        li.innerHTML = `${book.author} || ${book.name} || ${book.price}`
        div.appendChild(li)

        if (!book.author) {
            console.error('dont have author');
            div.removeChild(li);
        }
        if (!book.price) {
            div.removeChild(li);
            console.error('dont have price');
        }
    })






} catch (e) {

    console.log(e);
}